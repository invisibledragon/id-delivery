<?php

class DeliveryPlus_Shipping_Method extends WC_Shipping_Method {

	/**
	 * Constructor for your shipping class
	 *
	 * @access public
	 * @return void
	 */
	public function __construct($instance_id = 0) {
		$this->id                    = 'deliveryplus';
		$this->instance_id           = absint( $instance_id );
		$this->method_title          = __( 'DeliveryPlus' );
		$this->method_description    = __( 'Advanced Delivery Options by Invisible Dragon' );
		$this->supports              = array(
			'shipping-zones',
			'instance-settings',
		);
		$this->instance_form_fields = array(
			'title' => array(
				'title' 		=> __( 'Method Title' ),
				'type' 			=> 'text',
				'description' 	=> __( 'This controls the title which the user sees during checkout.' ),
				'default'		=> __( 'DeliveryPlus' )
			),
			'internal_name' => array(
			    'title'         => __( 'Internal Name' ),
                'type'          => 'text',
                'description'   => __( 'If specified, this appears inside the admin panel only for staff to understand which method this exactly is' ),
                'default'       => ''
            ),
			'description' => array(
				'title' => __('Description'),
				'type' => 'text',
				'description' => __('If specified, this additional information is displayed alongside the title in the checkout')
			),
			'tax_status' => array(
				'title'         => __( 'Tax status', 'woocommerce' ),
				'type'          => 'select',
				'class'         => 'wc-enhanced-select',
				'default'       => 'taxable',
				'options'       => array(
					'taxable' => __( 'Taxable', 'woocommerce' ),
					'none'    => _x( 'None', 'Tax status', 'woocommerce' ),
				),
			),
			'rate' => array(
				'title' 		=> __( 'Rate' ),
				'type' 			=> 'deliveryplus_rate',
				'description' 	=> __( 'This controls how the rate is calculated and displayed to the user' ),
				'default'		=> '0.00',
				'desc_tip'		=> true
			)
        );

        // Shipping classes
        if($instance_id > 0) {
            $shipping_classes = WC()->shipping()->get_shipping_classes();
            if (!empty($shipping_classes)) {
				$this->instance_form_fields['class_cost_mode'] = array(
					'title' => __('Shipping class cost mode'),
					'type' => 'select',
					'class' => 'wc-enhanced-select',
					'default' => 'per-item',
					'options' => array(
						'per-class' => __('Per Class'),
						'per-item' => __('Per Item')
					),
					'custom_attributes' => [
						'data-per-class' => __(
							'This cost will be added if the customer basket contains at'.
							' least 1 item with this shipping class. You can also use <code>[class_qty]</code> for'.
							' the total number of items in the basket with this shipping method (in addition to the' .
							' other variables)'
						),
						'data-per-item' => __(
							'This cost will be added for every item with this shipping class.'
						)
					]
				);
                foreach ($shipping_classes as $shipping_class) {
                    if (!isset($shipping_class->term_id)) {
                        continue;
                    }
                    $this->instance_form_fields['class_cost_' . $shipping_class->term_id] = array(
                        'title' => sprintf(__('"%s" shipping class cost'), esc_html($shipping_class->name)),
                        'type' => 'text',
                        'placeholder' => __('N/A')
                    );
                }

            }
        }

		$this->instance_form_fields['filter'] = array(
            'title'			=> __('Filter'),
            'type'			=> 'deliveryplus_filter',
            'description'	=> __('Add filtering rules to determine if this rate is to be displayed on the page'),
            'default'		=> '[]',
            'context'		=> 'shipping_method'
        );

		// GravityForms Integration
		if( class_exists('GFAPI') ) {

			$raw_forms = GFAPI::get_forms();
			$forms = array(
				null => __('No Form Selected')
			);
			foreach ($raw_forms as $form) {
				$forms[$form['id']] = $form['title'];
			}

			$this->instance_form_fields['form'] = array(
				'title' => __('Form'),
				'type' => 'select',
				'options' => $forms
			);

		}

		// Advanced Custom Fields integration
		if( function_exists('acf_render_fields') ) {

			$this->instance_form_fields['acf'] = array(
				'title' => __('Advanced'),
				'type' => 'deliveryplus_acf'
			);

		}

		$this->title         = $this->get_option( 'title' );
		$this->internal_name = $this->get_option( 'internal_name' );
		$this->rate          = $this->get_option( 'rate' );
		$this->tax_status    = $this->get_option( 'tax_status' );

		if($this->internal_name) {
		    $this->method_description = __('Internal Name: ') . '<code>' . $this->internal_name . '</code><br/>' . $this->method_description;
        }

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

		add_filter( 'woocommerce_shipping_' . $this->id . '_instance_settings_values', array( $this, 'acf_options' ) );

	}

	protected static $instance = null;
	public static function get_instance() {
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public static function activate() {
		$instance = static::get_instance();
		add_action( 'woocommerce_admin_field_deliveryplus_rate', array( $instance, 'admin_field_deliveryplus_rate' ) );
		add_action( 'woocommerce_admin_field_deliveryplus_filter', array( $instance, 'admin_field_deliveryplus_filter' ) );

		add_action( 'woocommerce_after_shipping_rate', [ static::class, 'after_shipping_rate' ] );
	}

	public static function after_shipping_rate($rate) {
		$current_shipping_method = WC()->session->get( 'chosen_shipping_methods' );
		if($rate->method_id === 'deliveryplus' && in_array($rate->id, $current_shipping_method) && $rate->meta_data['description']) {
			echo '<p class="id-delivery-selected-description">' . esc_html($rate->meta_data['description']) . '</p>';
		}
	}

	public function get_admin_options_html() {

		$wpby = __('WooCommerce Plugin by ');
		$enjoy = __('enjoying the plugin? give us a ko-fi');
		$dir = plugin_dir_url( __FILE__ );

		$html = <<<EOF
<div style="padding:10px;border:1px solid #ccc;background:#fff;display: flex">
    <div style="flex-grow:1">
        <a href="//invisibledragonltd.com/plugins/" target="_blank">
            ${wpby}
            <img style="height: 24px;vertical-align:middle" src="${dir}clogo.svg" />
        </a>
    </div>
    <div>
        <a href="https://ko-fi.com/invisibledragonltd" target="_blank">
			${enjoy}
        </a>
    </div>
</div>
EOF;
		$html .= parent::get_admin_options_html();

		return $html;

	}

	/**
	 * This function validates and saves the ACF options
	 */
	public function acf_options($opts) {

		if($this->instance_id == sanitize_text_field($_GET['instance_id'])) {

		    if( function_exists('acf_validate_save_post') ) { // ACF may not be installed
                if (acf_validate_save_post(true)) {
                    acf_save_post("woo_ship_" . $this->instance_id);
                }
            }

		}

		return $opts;

	}

	public function admin_field_deliveryplus_filter($args) {
		$this->settings[$args['id']] = $args['value'];
		echo $this->generate_deliveryplus_filter_html($args['id'], $args);
		unset($this->settings[$args['id']]);
	}

	public function generate_deliveryplus_filter_html($key, $args = null) {

		ob_start();

		echo '<div class="id-filtering">';

		DeliveryPlus_Filters::output_html($args);

		echo '</div>';

		$args['type'] = 'hidden';
		$html = $this->generate_text_html($key, $args);
		return str_replace('</legend>', '</legend>' . ob_get_clean(), $html);

	}

	public function admin_field_deliveryplus_rate($args) {
		$this->settings[$args['id']] = $args['value'];
		echo $this->generate_deliveryplus_rate_html($args['id'], $args);
		unset($this->settings[$args['id']]);
	}

	public function generate_deliveryplus_rate_html($key, $args = null) {

		global $shortcode_tags;

		$args['type'] = 'text';
		$html = $this->generate_text_html($key, $args);

		$info = '<div class="notice notice-info inline">';
		$info .= '<p><strong>' . __('Rate Formula Information') . '</strong></p>';
		$info .= '<p>' . __('This rate can be a formula using math operators including brackets, +, -, /, ^ and *') . '</p>';
		$info .= '<p>' . __('The following special variables can be used:') . '</p><ul>';

		$info .= '<li><code>[qty]</code> ' . __('The total quantity in the basket') . '</li>';
		$info .= '<li><code>[width]</code> ' . __('The total width of all items in the basket (If you have multiple quantity, each is counted seperately).') . '</li>';
		$info .= '<li><code>[height]</code> ' . __('The total height of all items in the basket.') . '</li>';
		$info .= '<li><code>[length]</code> ' . __('The total length of all items in the basket.') . '</li>';
		$info .= '<li><code>[flat_area]</code> ' . __('The total flat area (width * length) of all items in the basket.') . '</li>';
		$info .= '<li><code>[cost]</code> ' . __('The total in the basket (without shipping).') . '</li>';

		$info .= apply_filters('deliveryplus_cost_shortcodoes_help', '');

		$info .= '</ul>';
		$info .= '</div>';

		return str_replace('</fieldset>', $info . '</fieldset>', $html);

	}

	public function generate_deliveryplus_acf_html() {

		ob_start();

		$field_groups = acf_get_field_groups(array(
			'deliveryplus' => true
		));

		// bail early if no field groups
		if( empty($field_groups) ) {
			return;
		}

		// form data
		$post_id = 'woo_ship_' . $this->instance_id;

		acf_form_data(array(
			'screen'		=> 'deliveryplus',
			'post_id'		=> $post_id,
			'validation'	=> ($args['view'] == 'register') ? 0 : 1
		));

		// loop
		foreach( $field_groups as $field_group ) {

			// vars
			$fields = acf_get_fields( $field_group );

			// title
			if( $field_group['style'] === 'default' ) {
				echo '</table><h2>' . $field_group['title'] . '</h2><table class="form-table">';
			}

			// render
			acf_render_fields( $fields, $post_id, 'tr', $field_group['instruction_placement'] );

		}

		return ob_get_clean();

	}

	public function do_shortcode($ret, $tag, $attr, $m) {

		global $shortcode_tags;
		// Similar to Wordpress, but $content is swapped for the package
		return $m[1] . call_user_func( $shortcode_tags[ $tag ], $attr, $this->package, $tag ) . $m[6];

	}

	public function evaluate_cost($formula, $package) {

		global $shortcode_tags;

		include_once WC()->plugin_path() . '/includes/libraries/class-wc-eval-math.php';

		// Deal with decimal points
        $locale         = localeconv();
        $decimals       = array( wc_get_price_decimal_separator(), $locale['decimal_point'], $locale['mon_decimal_point'], ',' );

		// First calculate the totals for the package
		$total_quantity = 0;
		$total_width = 0;
		$total_height = 0;
		$total_length = 0;
		$total_weight = 0;
		$total_flat_area = 0;

		$additional_cost = [];
		$shipping_classes = [];

		foreach ( $package['contents'] as $item_id => $values ) {
			if ( $values['quantity'] > 0 && $values['data']->needs_shipping() ) {
				$total_quantity += $values['quantity'];

				// For each product, add in the width, height, length and weight
				// so we can calculate based on them
				$prod = $values['data'];
				$width = $prod->get_width('edit');
				if($width){ $total_width += $width * $values['quantity']; }
				$height = $prod->get_height('edit');
				if($height){ $total_height += $height * $values['quantity']; }
				$length = $prod->get_length('edit');
				if($width){ $total_length += $length * $values['quantity']; }

				// Now we can do some area calculations
				if($width && $length){
					$total_flat_area += ( $width * $length ) * $values['quantity'];
				}

				$weight = $prod->get_weight('edit');
				if($weight){ $total_weight += $weight * $values['quantity']; }

                $shipping_class = $values['data']->get_shipping_class();
				if(!empty($shipping_class)) {
                    $shipping_class = get_term_by( 'slug', $shipping_class, 'product_shipping_class' );
                    $class = $shipping_class->term_id;
					if(!array_key_exists($class, $shipping_classes)) {
						$shipping_classes[$class] = 0;
					}
					$shipping_classes[$class] += $values['quantity'];
                }
			}
		}

		foreach($shipping_classes as $class => $counter) {
			$charge = $this->get_option( 'class_cost_' . $class );
			// If this was never set, it will return per-item
			if($this->get_option('class_cost_mode', 'per-item') == 'per-item') {
				// Wrap it up so it will charge per item
				$charge = '(' . $charge . ') * [class_qty]';
			}
			$charge = str_replace( '[class_qty]', $counter, $charge );
			$additional_cost[] = $charge;
		}

		$old_shortcodes = $shortcode_tags;
		$shortcode_tags = array();

		// Action anyone listening to add their shortcodes now
		do_action('deliveryplus_cost_shortcodes');

		// Now we override how WordPress does filters so that we can pass down the package instead of content
		$this->package = $package;
		add_filter( 'pre_do_shortcode_tag', array( $this, 'do_shortcode' ), 0, 4 );

		$items = array(
			'[qty]',
			'[width]',
			'[height]',
			'[length]',
			'[flat_area]',
			'[weight]',
			'[cost]',
		);
		$replacements = array(
			$total_quantity,
			$total_width,
			$total_height,
			$total_length,
			$total_flat_area,
			$total_weight,
			$package['contents_cost'],
		);

		$sum = do_shortcode(str_replace( $items, $replacements, $formula ) );
		foreach($additional_cost as $cost) {
			$cost = do_shortcode( str_replace($items, $replacements, $cost) );
			$sum = '(' . $sum . ') + ' . $cost; // add on this formula
		}

		// Remove filter
		remove_filter( 'pre_do_shortcode_tag', array( $this, 'do_shortcode' ), 0 );

		$shortcode_tags = $old_shortcodes;

		// Remove whitespace from string.
        $sum = preg_replace( '/\s+/', '', $sum );

        // Remove locale from string.
        $sum = str_replace( $decimals, '.', $sum );

        // Trim invalid start/end characters.
        $sum = rtrim( ltrim( $sum, "\t\n\r\0\x0B+*/" ), "\t\n\r\0\x0B+-*/" );

        // Do the math.
        return ($sum ? WC_Eval_Math::evaluate( $sum ) : 0);

	}

	/**
	 * Filter package for specfic items which do not want certain delivery methods
	 *
	 * @param array $package
	 *
	 * @return bool
	 */
	public function filter_package( $package = array() ) {

		foreach ( $package['contents'] as $item_id => $values ) {
			$prod = $values['data'];


		}
		return true;

	}

	public function calculate_shipping( $package = array() ) {

		$rate = array(
			'id' => $this->get_rate_id(),
			'label' => $this->title,
			'cost' => 0,
			'package' => $package,
			'meta_data' => array(
				'gform' => $this->get_option('form'),
				'description' => $this->get_option('description')
			)
		);

		$filters = json_decode($this->get_option('filter'), true);
		if(!empty($filters) && count($package['contents']) > 0) {
			if(!DeliveryPlus_Filters::filter_package($filters, $package)) {
                return;
			}
		}

		$rate['cost'] = $this->evaluate_cost($this->rate, $package);

		if($rate['cost'] < 0 && $this->is_taxable()) {
			// Calculate negative tax rate if the amount is negative as WooCommerce doesn't support this
			// out of the box
			$rate['taxes'] = WC_Tax::calc_shipping_tax( abs($rate['cost']), WC_Tax::get_shipping_tax_rates() );
			foreach($rate['taxes'] as &$tax) {
				$tax = 0 - $tax;
			}
		}

		$this->add_rate($rate);

	}


}
