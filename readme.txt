=== DeliveryPlus ===
Contributors: invisibledragonltd
Donate link: https://ko-fi.com/invisibledragonltd
Tags: woocommerce, delivery, gravityforms, acf, advancedcustomfields
Requires at least: 6.0
Tested up to: 6.0
Requires PHP: 7.2
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

DeliveryPlus provides a delivery option with rate calculation and filter rules. Also integrates with Gravity Forms and Advanced Custom Fields.

== Description ==

DeliveryPlus was born out of a need for a clean, easy to use and native delivery method for WooCommerce. This plugin doesn't add any additional tabs to the WordPress menu bar, instead sitting neatly inside of WooCommerce's shipping options as it should, appearing like any other.

Each delivery option can opt to provide a method of calculation, with the available tools displayed on screen, and the ability to filter if the delivery option appears in the checkout under certain conditions.

The functionality can be extended if the plugin detects Gravity Forms installed, and allows you to pick a form to be displayed on the checkout if your delivery option is selected. Upon order submit, the details are saved and are visible inside of the order view of the WooCommerce admin.

Again, if you have Advanced Custom Fields installed, you can create custom fields which appear inside of the shipping admin area and, with custom code, can be used to create an advanced shipping section on a custom theme.

== Installation ==

The plugin installs in the standard WordPress fashion. Typically use the Plugins and then Add New and search for the plugin. Or, the manual way:

1. Upload the plugin directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Does it work with any other form plugin? =

Not at the moment

= Do I need the pro version of Advanced Custom Fields? =

No

= Does it work with any other custom fields plugin? =

Not at the moment

== Screenshots ==

1. A typical shipping method
2. A Gravity Form inside of checkout

== Changelog ==

= 1.0 =
* Initial Release

= 1.1 =
* Ability to remove conditions if added by mistake
* SubTotal condition added
* Bugs fixed
* Test Suite added

= 1.2 =
* Total number of items filter
* Each product can choose which delivery options apply to itself (optional)

= 1.2.1 =
* Bugfix

= 1.3 =
* Actually works without Advanced Custom Fields
* Rate per each shipping class

= 1.4 =
* Change which options apply to a product to override filters

= 1.5 =
* Allow overrides to be a filter

= 1.6 =
* Shipping class improve functionality
* Fix enabled bug
* Precise location filter

= 1.7 =
* Add shipping method description on frontend

== Upgrade Notice ==

= 1.0 =
Initial Release
