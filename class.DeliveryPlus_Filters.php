<?php

/**
 * This is a standard DeliveryPlus condition, specifying the value for this
 * condition should be entered into a textbox
 */
class DPF_Value {

	public $type = 'value';

	public function __construct($label) {
		$this->label = $label;
	}

	public function toJSON() {
		return array(
			'type' => $this->type,
			'label' => $this->label
		);
	}

}

/**
 * This is a variation of a DeliveryPlus conditon, whereby the value
 * needs to be picked from a dropdown list.
 */
class DPF_Pick_Value extends DPF_Value {

	public $type = 'pick';

}

abstract class DeliveryPlus_Filter_Set {

	public static $category = 'Unknown';
	public static function category_label(){ return __('Unknown'); }

	public static function activate() {

		add_filter( 'deliveryplus_categories', array( static::class, 'add_category' ), 10, 2 );
		add_filter( 'deliveryplus_values_' . static::$category, array( static::class, 'get_values' ), 10, 2 );
		add_filter( 'deliveryplus_conditions_' . static::$category, array( static::class, 'get_conditions' ) );

		// Now associate a filter function for each condition
		$conditions = static::get_conditions(array());
		foreach($conditions as $key => $val) {
		    if( method_exists(static::class, 'do_' . $key) ) {
                add_filter('deliveryplus_filter_' . static::$category . '_' . $key, array(static::class, 'do_' . $key), 10, 3);
            }
		}

	}

	abstract public static function get_conditions($conditions);

	public static function get_values($ret, $condition){
		// TODO by sub-function
		return $ret;
	}

	public static function add_category($filters, $context) {
		$filters[static::$category] = static::category_label();
		return $filters;
	}
}


class DeliveryPlus_Filters {

	public static function activate() {
		add_action( 'wp_ajax_deliveryplus_get_conditions', array( static::class, 'get_conditions_ajax' ) );
		add_action( 'wp_ajax_deliveryplus_get_values', array( static::class, 'get_values_ajax' ) );

		// Activate default filters
		require_once "class.DeliveryPlus_Filter_Number.php";
		require_once "class.DeliveryPlus_Filter_Weight.php";
		require_once "class.DeliveryPlus_Filter_Category.php";
		require_once "class.DeliveryPlus_Filter_SubTotal.php";
		require_once "class.DeliveryPlus_Filter_Count.php";
        require_once "class.DeliveryPlus_Filter_Override.php";
		require_once "class.DeliveryPlus_Filter_ShippingClass.php";
		require_once 'class.DeliveryPlus_Filter_Distance.php';

		DeliveryPlus_Filter_Category::activate();
		DeliveryPlus_Filter_SubTotal::activate();
		DeliveryPlus_Filter_Weight::activate();
		DeliveryPlus_Filter_Count::activate();
        DeliveryPlus_Filter_Override::activate();
		DeliveryPlus_Filter_ShippingClass::activate();
		DeliveryPlus_Filter_Distance::activate();
	}

	public static function get_values_ajax() {
		$category = sanitize_text_field($_GET['category']);
		if(!$category) return wp_die('Invalid Request', 'DeliveryPlus', array( 'response' => 400 ));
		$condition = sanitize_text_field($_GET['condition']);
		if(!$condition) return wp_die('Invalid Request', 'DeliveryPlus', array( 'response' => 400 ));

		$json = static::get_values($category, $condition);
		header("Content-Type: application/json");
		echo json_encode($json);
		return wp_die();
	}

	public static function get_categories($context = '') {

		$categories = apply_filters( 'deliveryplus_categories', array(), $context );

		return $categories;

	}

	public static function get_values($category, $condition) {

		$ret = [];
		$ret = apply_filters( 'deliveryplus_values_' . $category, $ret, $condition );

		return $ret;

	}

	public static function get_conditions_ajax() {
		$category = sanitize_text_field($_GET['category']);
		if(!$category) return wp_die('Invalid Request', 'DeliveryPlus', array( 'response' => 400 ));

		$json = static::get_conditions($category);
		header("Content-Type: application/json");
		echo json_encode($json);
		return wp_die();
	}

	public static function get_conditions($category) {

		$ret = [];
		$ret = apply_filters( 'deliveryplus_conditions_' . $category, $ret );

		return $ret;

	}

	public static function output_html($args) {
		require("includes/filters.php");
	}

	public static function filter_block_package( $block, $package ) {

		if(empty($block)) {
			return true;
		}

		foreach( $block as $rule ) {

			$filter = 'deliveryplus_filter_' . $rule['category'] . '_' . $rule['condition'];
			if(!apply_filters($filter, false, $package, $rule)) {
				return false;
			}

		}

		return true;

	}

	public static function filter_package( $filters, $package ) {

		if(empty($filters)) {
			return true;
		}

		foreach($filters as $block) {

			// For each block of rules, all of the conditions must match
			$result = static::filter_block_package($block, $package);
			if($result) {
				return true;
			}

		}

		return false;

	}

}
