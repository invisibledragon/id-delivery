<?php

class DeliveryPlus_Filter_Category extends DeliveryPlus_Filter_Set {

	public static $category = 'Category';

	public static function category_label(){ return __('Category'); }

	public static function activate() {
		parent::activate();
		add_filter( 'deliveryplus_filter_' . static::$category . '_1', array( static::class, 'at_least_one' ), 10, 3 );
		add_filter( 'deliveryplus_filter_' . static::$category . '_!', array( static::class, 'none' ), 10, 3 );
		add_filter( 'deliveryplus_filter_' . static::$category . '_!a', array( static::class, 'not_all' ), 10, 3 );
		add_filter( 'deliveryplus_filter_' . static::$category . '_a', array( static::class, 'all' ), 10, 3 );
	}

	public static function get_conditions($conditions) {
		$ret['1'] = new DPF_Pick_Value(__('at least one in'));
		$ret['a'] = new DPF_Pick_Value(__('all must be in'));
		$ret['!'] = new DPF_Pick_Value(__('none must be in'));
		$ret['!a'] = new DPF_Pick_Value(__('not all in'));
		return $ret;
	}

	public static function get_values($ret, $condition) {
		$raw = get_categories(array('taxonomy' => 'product_cat', 'hide_empty' => false));
		foreach($raw as $cat) {
			$ret[$cat->term_id] = wp_specialchars_decode( $cat->name );
		}
		return $ret;
	}

	public static function match_line($value, $product_id) {
		$children = get_term_children($value, 'product_cat');
		$children[] = $value;
		return has_term($children, 'product_cat', $product_id);
	}

	public static function all($val, $package, $rule){

		$counter = 0;
		foreach( $package['contents'] as $line ) {
			if( static::match_line( $rule['value'], $line['product_id'] ) ) {
				$counter += 1;
			}
		}

		if($counter == count($package['contents'])){
			return true;
		}

		return $val;

	}

	public static function not_all($val, $package, $rule){

		$counter = 0;
		foreach( $package['contents'] as $line ) {
			if( static::match_line( $rule['value'], $line['product_id'] ) ) {
				$counter += 1;
			}
		}

		if($counter < count($package['contents'])){
			return true;
		}

		return $val;

	}

	public static function none($val, $package, $rule){

		foreach( $package['contents'] as $line ) {
			if( static::match_line( $rule['value'], $line['product_id'] ) ) {
				return false;
			}
		}

		return true;

	}

	public static function at_least_one($val, $package, $rule){

		foreach( $package['contents'] as $line ) {
			if( static::match_line( $rule['value'], $line['product_id'] ) ) {
				return true;
			}
		}

		return $val;

	}

}