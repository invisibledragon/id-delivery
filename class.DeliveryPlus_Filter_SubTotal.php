<?php

class DeliveryPlus_Filter_SubTotal extends DeliveryPlus_Filter_Number {

	public static $category = 'SubTotal';
	public static function category_label(){ return __('Sub-Total'); }

	public static function get_value($val, $package, $rule) {
		return $package['contents_cost'];
	}

}