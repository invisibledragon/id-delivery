<?php

class DeliveryPlus_Filter_Count extends DeliveryPlus_Filter_Number {

	public static $category = 'Count';
	public static function category_label(){ return __('Basket Count'); }

	public static function get_value($val, $package, $rule) {
		$qty = 0;

		foreach ( $package['contents'] as $item_id => $values ) {
			$qty += $values['quantity'];
		}

		return $qty;
	}

}