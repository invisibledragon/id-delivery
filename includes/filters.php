<style>
	.id-filter-ui table select { width: 100% !important;  }
	.id-filter-ui table { max-width: 700px; margin-left: 1rem; margin-top: 1.5rem;  }
	.id-filter-ui th { padding: 20px; }
    .id-filter-ui td { width: 33%; }
    .id-filter-ui .footer { padding-top: 1.5rem; }
    .id-filter-ui .template { display: none; }
    .id-filter-ui .id-remove { color: #a00; }
    .id-filter-ui .id-remove:hover{ color: #c30000; }
</style>
<div class="id-filter-ui">

    <p class="intro"><strong><?= __('At least one of the following blocks match'); ?></strong></p>

    <div class="id-filter-set template">
        <table class="widefat">
            <thead>
                <tr>
                    <th colspan="4">
                        <?= __('All of the following conditions match:'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="template">
                    <td>
                        <select class="id-filter-category">
                            <?php /** @var array $args */
                            $categories = DeliveryPlus_Filters::get_categories($args['context']); foreach($categories as $key => $label): ?>
                            <option value="<?= $key; ?>"><?= $label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <select class="id-filter-condition"></select>
                    </td>
                    <td>
                        <span class="id-filter-value">...</span>
                    </td>
                    <td>
                        <a href="#" class="id-remove id-remove-row" title="<?= __('Remove this row'); ?>">
                            <span class="dashicons dashicons-trash"></span>
                        </a>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="3">
                        <a class="button button-primary id-add-new-and"><?= __('Add new'); ?></a>
                    </th>
                    <td>
                        <a href="#" class="id-remove id-remove-block" title="<?= __('Remove this block'); ?>">
                            <span class="dashicons dashicons-trash"></span>
                        </a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

    <p class="footer"><a class="button button-primary id-add-new-or"><?= __('Add new'); ?></a></p>

</div>
