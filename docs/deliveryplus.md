# DeliveryPlus Class

DeliveryPlus adds a class available to other plugins and themes, simply named `DeliveryPlus` which provides
a bunch of useful functions!

## get_field($rate, $key)

Parameters:

- $rate instanceof [WC_Shipping_Rate](https://docs.woocommerce.com/wc-apidocs/class-WC_Shipping_Rate.html)
- $key instanceof String

This returns an Advanced Custom Field's value for a DeliveryPlus shipping rate.

**Note:** Requires Advanced Custom Fields to be installed.

Example:

```html
<p class="card-text">
    <?= DeliveryPlus::get_field($rate, 'description'); ?>
</p>
```

## have_rows($rate, $key)

Parameters:

- $rate instanceof [WC_Shipping_Rate](https://docs.woocommerce.com/wc-apidocs/class-WC_Shipping_Rate.html)
- $key instanceof String

This processes an Advanced Custom Field which has multiple subvalues for a DeliveryPlus shipping rate. This works
in a similar way to the top level `have_rows` function from Advanced Custom Fields.

Using the top-level `the_row` function from Advanced Custom Fields works fine with this method.

**Note:** Requires Advanced Custom Fields to be installed.

Example:

```html
<ul class="selling-points">
    <?php while(DeliveryPlus::have_rows($rate, 'selling_points')): the_row(); ?>
    <li>
        <i class="fa fa-check"></i>
        <?= the_sub_field('usp'); ?>
    </li>
    <?php endwhile; ?>
</ul>
```

## methods_for_item($product, $destination = null)

Parameters:

- $product instanceof [WC_Product](https://docs.woocommerce.com/wc-apidocs/class-WC_Product.html)
- $destination optional instance of array. Example: `[ 'country' => 'GB' ]`

This returns a list of rates available for shipping just 1 instance of the product. This is useful if you want to
list the delivery methods on your single product page.

Example:

```php
global $product;

$methods = DeliveryPlus::methods_for_item($product, [
    'country' => 'GB'
]);

foreach($methods as $rate):

	echo '<p>' . DeliveryPlus::get_field($rate, 'description') . '</p>';

endforeach;
```
