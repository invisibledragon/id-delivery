<?php

class DeliveryPlus_Filter_Weight extends DeliveryPlus_Filter_Number {

	public static $category = 'Weight';

	public static function category_label(){ return __('Weight'); }

	public static function get_value($val, $package, $rule) {
		$total_weight = 0;

		foreach ( $package['contents'] as $item_id => $values ) {
			$prod = $values['data'];

			if ( $values['quantity'] > 0 && $values['data']->needs_shipping() ) {
				$weight = $prod->get_weight('edit');
				if($weight){ $total_weight += $weight * $values['quantity']; }
			}
		}

		return $total_weight;
	}

}
