<?php

/**
 * Class ReplacementTest
 *
 * @package DeliveryPlus
 */

class FakeReplacementObject {

	public function needs_shipping(){ return true; }
	public function get_weight($mode){ return 1; }
	public function get_width($mode){ return 2; }
	public function get_height($mode){ return 3; }
	public function get_length($mode){ return 4; }
	public function get_shipping_class(){}

}

/**
 * Tests if replacement variables are calculated correctly
 */
class ReplacementTest extends WP_UnitTestCase {

	public function get_sample_package() {

		return array(
			'contents' => array(
				array(
					'quantity' => 5,
					'data' => new FakeReplacementObject()
				),
				array(
					'quantity' => 1,
					'data' => new FakeReplacementObject()
				),
				array(
					'quantity' => 1,
					'data' => new FakeReplacementObject()
				),
			),
			'contents_cost' => 40.69
		);

	}

	/**
	 * Test the quantity is counted correctly
	 */
	public function test_quantity() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[qty]', $this->get_sample_package()), 7);
	}

	/**
	 * Test the weight is counted correctly
	 */
	public function test_weight() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[weight]', $this->get_sample_package()), 7);
	}

	/**
	 * Test the width is counted correctly
	 */
	public function test_width() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[width]', $this->get_sample_package()), 14);
	}

	/**
	 * Test the height is counted correctly
	 */
	public function test_height() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[height]', $this->get_sample_package()), 21);
	}

	/**
	 * Test the length is counted correctly
	 */
	public function test_length() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[length]', $this->get_sample_package()), 28);
	}

	/**
	 * Test the area is counted correctly
	 */
	public function test_area() {
		$method = new DeliveryPlus_Shipping_Method();

		// l*w = 4*2 = 8
		// qty * area = 7 * 8 = 56
		$this->assertEquals($method->evaluate_cost('[flat_area]', $this->get_sample_package()), 56);
	}

	/**
	 * Test the length is cost correctly
	 */
	public function test_cost() {
		$method = new DeliveryPlus_Shipping_Method();

		$this->assertEquals($method->evaluate_cost('[cost]', $this->get_sample_package()), 40.69);
	}

}
