<?php

require_once "class.DeliveryPlus_Filters.php";
require_once "class.DeliveryPlus_Filter_Number.php";
require_once "class.DeliveryPlus_Filter_Weight.php";

/**
 * Class ReplacementTest
 *
 * @package DeliveryPlus
 */

class FakeWeightObject {

	public function needs_shipping(){ return true; }
	public function get_weight($mode){ return 1; }

}

/**
 * Tests if weight filters work correctly
 */
class WeightFilterTest extends WP_UnitTestCase {

	public function get_sample_package() {

		// Weight is 7
		return array(
			'contents' => array(
				array(
					'quantity' => 5,
					'data' => new FakeWeightObject()
				),
				array(
					'quantity' => 1,
					'data' => new FakeWeightObject()
				),
				array(
					'quantity' => 1,
					'data' => new FakeWeightObject()
				),
			),
			'contents_cost' => 40.69
		);

	}

	/**
	 * Test greater than
	 */
	public function test_gt() {
		// 7 > 7
		$this->assertEquals(7, DeliveryPlus_Filter_Weight::do_gt(7, $this->get_sample_package(), null));
		// 7 > 6
		$this->assertTrue(DeliveryPlus_Filter_Weight::do_gt(6, $this->get_sample_package(), null));
	}

	/**
	 * Test greater than or equal to
	 */
	public function test_gte() {
		// 7 >= 6
		$this->assertEquals(6, DeliveryPlus_Filter_Weight::do_gte(6, $this->get_sample_package(), null));
		// 7 >= 7
		$this->assertTrue(DeliveryPlus_Filter_Weight::do_gte(7, $this->get_sample_package(), null));
	}

	/**
	 * Test less than
	 */
	public function test_lt() {
		// 7 < 7
		$this->assertEquals(7, DeliveryPlus_Filter_Weight::do_lt(7, $this->get_sample_package(), null));
		// 7 < 8
		$this->assertTrue(DeliveryPlus_Filter_Weight::do_lt(8, $this->get_sample_package(), ['value' => 8]));
	}

	/**
	 * Test less than or equal to
	 */
	public function test_lte() {
		// 7 <= 6
		$this->assertEquals(true, DeliveryPlus_Filter_Weight::do_lte(6, $this->get_sample_package(), null));
		// 7 <= 7
		$this->assertTrue(DeliveryPlus_Filter_Weight::do_lte(7, $this->get_sample_package(), ['value' => 7]));
	}

}
