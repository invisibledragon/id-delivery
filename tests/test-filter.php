<?php

require_once "class.DeliveryPlus_Filters.php";
require_once "class.DeliveryPlus_Filter_Number.php";
require_once "class.DeliveryPlus_Filter_SubTotal.php";

/**
 * Tests if filters work correctly
 */
class FilterTest extends WP_UnitTestCase {

	/**
	 * Test contents cost
	 */
	public function test_contents_cost() {
		$package = array(
			'contents_cost' => 20.49
		);

		// 20.49 > 21
		$this->assertEquals(21, DeliveryPlus_Filter_SubTotal::do_gt(21, $package, null));
		// 20.49 > 20
		$this->assertTrue(DeliveryPlus_Filter_SubTotal::do_gt(20, $package, null));
	}

}