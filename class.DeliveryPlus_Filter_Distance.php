<?php

class DeliveryPlus_Filter_Distance extends DeliveryPlus_Filter_Set {

	public static $category = 'distance';
	public static function category_label(){ return __('Distance'); }

	public static function activate() {
		parent::activate();

		add_filter( 'woocommerce_shipping_settings', [ static::class, 'shipping_settings' ] );
	}

	// Add a setting for API key to allow distance to work
	public static function shipping_settings($settings) {
		$settings[] = array(
			'title' => __( 'DeliveryPlus' ),
			'type'  => 'title',
			'id'    => 'deliveryplus',
		);
		$settings[] = [
			'id' => 'deliveryplus_distance_apikey',
			'title' => __('Maptiler API Key'),
			'desc' => __(
				'DeliveryPlus uses an external service called Maptiler when Distance calculations are used. '.
				'An API key from this service is required in order to use this service'),
			'type' => 'text'
		];
		$settings[] = array(
			'type' => 'sectionend',
			'id'   => 'deliveryplus',
		);
		return $settings;
	}

	public static function get_conditions($conditions) {
		$conditions['within'] = new DFP_Distance(__('within km of'));
		$conditions['outside'] = new DFP_Distance(__('outside km of'));
		return $conditions;
	}

	/**
	 * Calculates the great-circle distance between two points, with
	 * the Vincenty formula.
	 *
	 * Thanks to https://stackoverflow.com/a/10054282/230419
	 *
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longitude of target point in [deg decimal]
	 * @param float $earthRadius Mean earth radius in [m]
	 * @return float Distance between points in [m] (same as earthRadius)
	 */
	public static function vincentyGreatCircleDistance(
		$latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
	{
		// convert from degrees to radians
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$lonDelta = $lonTo - $lonFrom;
		$a = pow(cos($latTo) * sin($lonDelta), 2) +
			pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
		$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

		$angle = atan2(sqrt($a), $b);
		return $angle * $earthRadius;
	}

	public static function get_value($val, $package, $rule) {
		$address = implode( ', ', array_filter([
			$package['destination']['address_1'],
			$package['destination']['address_2'],
			$package['destination']['city'],
			$package['destination']['state'],
			$package['destination']['postcode'],
			WC()->countries->countries[ $package['destination']['country'] ]
		]));
		$url = 'https://api.maptiler.com/geocoding/' . urlencode($address) . '.json';
		if( $response = get_transient( 'deliveryplus_distance_request_'  . sha1($url) ) ) {
			$json = json_decode( $response, true );
		} else {
			$response = wp_remote_get( $url . '?key=' . get_option('deliveryplus_distance_apikey'));
			if ($response['response']['code'] != 200) {
				return null;
			}
			// Save for 1 hour
			set_transient( 'deliveryplus_distance_request_' . sha1($url), $response['body'], 60 * 60 );
			$json = json_decode($response['body'], true);
		}
		$center = $json['features'][0]['center'];
		return [ $center[1], $center[0] ];
	}

	public static function do_outside($val, $package, $rule){
		$marker = static::get_value($val, $package, $rule);
		if(!$marker) {
			return false;
		}
		$rule_parts = explode("|", $rule['value']);
		$distance = static::vincentyGreatCircleDistance(
				$marker[0],
				$marker[1],
				$rule_parts[1],
				$rule_parts[2]
			) / 1000;

		if($distance >= $rule_parts[0]) {
			return true;
		}

		return $val;
	}

	public static function do_within($val, $package, $rule){
		$marker = static::get_value($val, $package, $rule);
		if(!$marker) {
			return false;
		}
		$rule_parts = explode("|", $rule['value']);
		$distance = static::vincentyGreatCircleDistance(
			$marker[0],
			$marker[1],
			$rule_parts[1],
			$rule_parts[2]
		) / 1000;

		if($distance <= $rule_parts[0]) {
			return true;
		}

		return $val;
	}

}

/**
 * This is a variation of a DeliveryPlus conditon, whereby the value
 * needs to be entered in several parts
 */
class DFP_Distance extends DPF_Value {

	public $type = 'distance';

}
