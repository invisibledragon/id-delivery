<?php

abstract class DeliveryPlus_Filter_Number extends DeliveryPlus_Filter_Set {

	public static function get_conditions($conditions) {
		$ret['gt'] = new DPF_Value(__('greater than'));
		$ret['gte'] = new DPF_Value(__('greater than or equal to'));
		$ret['lt'] = new DPF_Value(__('less than'));
		$ret['lte'] = new DPF_Value(__('less than or equal to'));
		return $ret;
	}

	abstract public static function get_value($val, $package, $rule);

	public static function do_gt($val, $package, $rule){

		$subtotal = static::get_value($val, $package, $rule);
		if($subtotal > $rule['value']) {
			return true;
		}

		return $val;

	}

	public static function do_gte($val, $package, $rule){

		$subtotal = static::get_value($val, $package, $rule);
		if($subtotal >= $rule['value']) {
			return true;
		}

		return $val;

	}

	public static function do_lt($val, $package, $rule){

		$subtotal = static::get_value($val, $package, $rule);
		if($subtotal < $rule['value']) {
			return true;
		}

		return $val;

	}

	public static function do_lte($val, $package, $rule){

		$subtotal = static::get_value($val, $package, $rule);
		if($subtotal <= $rule['value']) {
			return true;
		}

		return $val;

	}

}
