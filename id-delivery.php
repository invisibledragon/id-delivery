<?php
/*
Plugin Name: DeliveryPlus by Invisible Dragon
Plugin URI: https://invisibledragonltd.com/plugins/deliveryplus/
Description: Advanced Delivery options for WooCommerce
Author: Invisible Dragon
Author URI: https://invisibledragonltd.com/
License: Private
Version: 1.7
Requires PHP: 5.6

WC requires at least: 6.9
WC tested up to: 6.9.2
*/

define('ID_DELIVERY_VERSION', '1.8');

add_action( 'plugins_loaded', array( 'DeliveryPlusPlugin', 'get_instance' ) );

require_once "class.DeliveryPlus_Filters.php";

/**
 * Frontend API for DeliveryPlus
 */
class DeliveryPlus {

	public static function get_field($rate, $key) {

		if( !( $rate instanceof WC_Shipping_Rate ) ) {
			throw new Exception('wrong item sent');
		}

		return get_field($key, 'woo_ship_' . $rate->get_instance_id());

	}

	public static function have_rows($rate, $key) {

		if( !( $rate instanceof WC_Shipping_Rate ) ) {
			throw new Exception('wrong item sent');
		}

		return have_rows($key, 'woo_ship_' . $rate->get_instance_id());

	}

	public static function methods_for_item($product, $destination = null) {

		$package = [
			'contents' => [
				'single' => [
					'quantity' => 1,
					'data' => $product
				]
			],
			'destination' => $destination
		];

		$results = WC()->shipping()->calculate_shipping_for_package($package, time());

		return $results['rates'];

	}

}

class DeliveryPlusPlugin {

	protected static $instance = null;

	public $plugin_name = 'id-delivery';

	public static function get_instance() {
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	private $_submit_order_id = null;
	private $_submit_order_msg = null;

	private function __construct() {

	    // Initialization stuff
		add_filter( 'plugin_row_meta', array( $this, 'check_for_recommendations' ), 10, 2 );
		add_action( 'woocommerce_shipping_init', array( $this, 'load_shipping' ) );
		add_filter( 'woocommerce_shipping_methods', array( $this, 'add_shipping' ) );

        // Order Page
        add_action( 'woocommerce_before_order_itemmeta', array( $this, 'before_order_itemmeta' ), 10, 2 );

		// GravityForms integration
		add_action( 'woocommerce_review_order_after_shipping', array( $this, 'review_order' ) );
		add_action( 'woocommerce_after_checkout_validation', array( $this, 'submit_order' ), 999999, 2 );
		add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'save_order' ) );
		add_action( 'woocommerce_admin_order_data_after_shipping_address', array( $this, 'show_entry' ) );
		add_action( 'woocommerce_thankyou', array( $this, 'show_confirmation_message' ), 1 );


		// Advanced Custom Fields integration
        add_action( 'admin_init', array( $this, 'enqueue_acf' ) );
		add_filter( 'acf/location/rule_types', array( $this, 'acf_location_rules_types' ) );
		add_filter( 'acf/location/rule_values/deliveryplus', array( $this, 'acf_location_rule_values' ) );
		add_filter( 'acf/location/rule_match/deliveryplus', array( $this, 'acf_location_rule_match' ), 10, 4 );

		DeliveryPlus_Filters::activate();

		add_action( 'before_woocommerce_init', [ $this, 'before_woocommerce_init' ] );

	}

	public function before_woocommerce_init() {
		if ( class_exists( \Automattic\WooCommerce\Utilities\FeaturesUtil::class ) ) {
			\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'custom_order_tables', __FILE__, false );
			\Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility( 'remote_logging', __FILE__, false );
		}
	}

	public function before_order_itemmeta($item_id, $item) {

	    if(!$item instanceof WC_Order_Item_Shipping) return;
	    $method =  WC_Shipping_Zones::get_shipping_method($item->get_instance_id());
        if(!$method instanceof DeliveryPlus_Shipping_Method) return;

        if($method->internal_name) {
            echo '<code>' . esc_html($method->internal_name) . '</code>';
        }

    }

	public function show_confirmation_message( $order_id ) {

		remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

		$comments = get_comments(array(
			'meta_key' => 'source',
			'meta_value' => 'gravityforms_confirm',
			'post_id' => $order_id,
			'approve' => 'approve',
			'type' => ''
		));

		foreach($comments as $comment) {
			echo $comment->comment_content;
		}

		add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );

	}

	public function show_entry( $order ) {

		$entry_id = get_post_meta( $order->get_id(), 'deliveryplus_gf_entry', true );

		if( $entry_id ) {

			$entry = GFAPI::get_entry($entry_id);
			$form = GFAPI::get_form($entry['form_id']);

			echo '<p><strong>' . esc_html($form['title']) . '</strong></p>';

			echo GFCommon::get_submitted_fields( $form, $entry );

		}

	}

	public function set_note_self( $args ) {

		$args['comment_author'] = 'Gravity Forms';
		$args['comment_meta'] = array(
			// Set this here otherwise we trigger actions we may not actually want!
			'is_customer_note' => 1,
			'source' => 'gravityforms_confirm'
		);
		return $args;

	}

	public function add_gf_message($order_id, $message) {
		add_filter( 'woocommerce_new_order_note_data', array( $this, 'set_note_self' ) );

		$order = wc_get_order(  $order_id );
		$order->add_order_note( $this->_submit_order_msg );


		remove_filter( 'woocommerce_new_order_note_data', array( $this, 'set_note_self' ) );
	}

	public function save_order($order_id) {

		if($this->_submit_order_id) {
			update_post_meta( $order_id, 'deliveryplus_gf_entry', $this->_submit_order_id );
		}

		if($this->_submit_order_msg) {

			$this->add_gf_message( $order_id, $this->_submit_order_msg );

		}

	}

	public function submit_order($data, $errors) {

		$notices = WC()->session->get( 'wc_notices', array() );
		if( wc_notice_count( 'error' ) > 0 || !empty($errors->errors) ) {
			return; // Don't bother if the form is going to fail basically
		}

		if(class_exists('GFAPI')) {

			require_once( GFCommon::get_base_path() . '/form_display.php' );

			$methods = WC()->cart->calculate_shipping();

			foreach($methods as $method) {

				if($method->method_id == 'deliveryplus' && $method->get_meta_data()['gform']) {

					$form_id = $method->get_meta_data()['gform'];
					$form = GFAPI::get_form($form_id);
					$response = GFAPI::submit_form($form_id, $_POST['deliveryplus_shipping_form']);

					if( $response['is_valid'] ) {

						// Success!
						$this->_submit_order_id = $response['entry_id'];
						$this->_submit_order_msg = $response['confirmation_message'];

					} else {

						// Failure
						wc_add_notice(
							str_replace('[form]', $form['title'], __('[form] has errors:') ), 'error' );

					}

				}

			}

		}

	}

	public function inline_gravityform($form, $form_key) {
		// If Ajax update, then push GravityForms into displaying properly
		$origpost = $_POST;
		if($_POST['post_data']) {
			$items = array();
			parse_str( wp_unslash( $_POST['post_data'] ), $items );

			if($items[$form_key]) {
				foreach($items[$form_key] as $key => $value) {
					$_POST[$key] = $value;
				}
			}
		}

		add_filter( 'gform_submit_button', '__return_false' );

		require_once( GFCommon::get_base_path() . '/form_display.php' );
		$html = GFFormDisplay::get_form($form['id'], false, false, null, false);
		$html = str_replace('<form', '<div', $html);
		$html = str_replace('</form>', '</div>', $html);
		// All name attributes need prefixing to prevent clashes
		$html = preg_replace('/name\=["\']([a-z\_0-9A-Z]+)["\']/', 'name="' . $form_key . '[${1}]"', $html);

		remove_filter( 'gform_submit_button', '__return_false' );

		$_POST = $origpost;

		return $html;
	}

	public function review_order() {

		if(class_exists('GFAPI')) {

			require_once( GFCommon::get_base_path() . '/form_display.php' );

			$methods = WC()->cart->calculate_shipping();

			foreach($methods as $method) {

				if($method->method_id == 'deliveryplus' && $method->get_meta_data()['gform']) {

					$form_id = $method->get_meta_data()['gform'];
					$form = GFAPI::get_form($form_id);

					$html = $this->inline_gravityform($form, 'deliveryplus_shipping_form');

					echo '<tr class="deliveryplus_gf"><th>' . $form['title'] . '</th><td>' . $html . '</td></tr>';

				}

			}

		}

	}

	public function enqueue_acf() {

		if(isset($_GET['page']) && $_GET['page'] == 'wc-settings' && isset($_GET['tab']) && $_GET['tab'] == 'shipping' && isset($_GET['instance_id'])) {
		    // Add ACF is we're trying to edit our shipping method and it's available
			$method = WC_Shipping_Zones::get_shipping_method( sanitize_text_field( $_GET['instance_id'] ) );
			if($method && $method->id == 'deliveryplus') {
				$this->enqueue_admin_script();

				if(function_exists('get_field')) {
					acf_enqueue_scripts();
				}
			}
		}

	}

	public function enqueue_admin_script() {
		wp_enqueue_script( 'deliveryplus_admin', plugin_dir_url( __FILE__ ) . '/admin.js', array( 'jquery' ), '1.0.1',
			true );
	}

	public function acf_location_rules_types($types) {

		if(!array_key_exists("Invisible Dragon", $types)) {
			$types["Invisible Dragon"] = array();
		}

		$types["Invisible Dragon"]["deliveryplus"] = "DeliveryPlus";

		return $types;

	}

	public function acf_location_rule_match( $match, $rule, $options, $field_group )
    {

        // Basically return true if the options we gave above is true (and the user selected this to appear flex only)
        if ( $rule[ 'param' ] == 'deliveryplus' && $options[ 'deliveryplus' ] == true ) {
            return true;
        }

        return $match;

    }

	public function acf_location_rule_values($rules) {
		return [ 1 => "DeliveryPlus" ];
	}

	public function add_shipping($methods) {
		$methods['deliveryplus'] = 'DeliveryPlus_Shipping_Method';
		return $methods;
	}

	public function load_shipping() {
		require_once "class.DeliveryPlus_Shipping_Method.php";
		DeliveryPlus_Shipping_Method::activate();

	}

	public function notice($notice){
		return '<div class="notice notice-warning inline"><p>' . $notice . '</p></div>';
	}

	public function check_for_recommendations($plugin_meta, $plugin_file ) {

		if($plugin_file != 'id-delivery/id-delivery.php') return $plugin_meta;

		$recommended = array();

		if(!class_exists('GFAPI')) {
			$recommended[] = 'Gravity Forms';
		}

		if(!function_exists('get_field')) {
			$recommended[] = 'Advanced Custom Fields';
		}

		if(!class_exists('WooCommerce')) {
			$recommended[] = 'WooCommerce';
		}

		if(!empty($recommended)){
			$plugin_meta[-1] = $this->notice(
				__('The following plugins are recommended for use with DeliveryPlus:') .
				'<ul><li>' .
				implode('</li><li>', $recommended) . '</li></ul>');
		}

		return $plugin_meta;


	}

}

