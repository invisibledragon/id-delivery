<?php

class DeliveryPlus_Filter_Override extends DeliveryPlus_Filter_Set {

    public static $category = 'Override';

    public static function category_label(){ return __('Has Shipping Method'); }

    public static function get_conditions($conditions) {
        $ret['1'] = new DPF_Pick_Value(__('at least 1 opted in'));
        $ret['a'] = new DPF_Pick_Value(__('all must be opted in'));
        $ret['none'] = new DPF_Pick_Value(__('none must be opted in'));
        $ret['na'] = new DPF_Pick_Value(__('not all opted in'));
        return $ret;
    }

    public static function get_values($ret, $condition) {

        $zones = WC_Shipping_Zones::get_zones();
        foreach($zones as $zone) {

            foreach ($zone['shipping_methods'] as $shipping_method) {

                if (!$shipping_method instanceof DeliveryPlus_Shipping_Method) return;

                $title = $shipping_method->title;

                if($shipping_method->internal_name) {
                    $title .= ' (' . $shipping_method->internal_name . ')';
                }
                $ret[$shipping_method->instance_id] = $title;

            }
        }
        return $ret;

    }

    public static function match_line($value, $product_id, $product) {

        if(!$product_id) return apply_filters('idd_override_default_opted_in', true);

        $opted_in = $product->get_meta('idd_options');
        if(!is_array($opted_in)) return apply_filters('idd_override_default_opted_in', true);
        return in_array($value, $opted_in);

    }

    public static function do_a($val, $package, $rule){

        $counter = 0;
        foreach( $package['contents'] as $line ) {
            if( static::match_line( $rule['value'], $line['product_id'], $line['data'] ) ) {
                $counter += 1;
            }
        }

        if($counter == count($package['contents'])){
            return true;
        }

        return $val;

    }

    public static function do_na($val, $package, $rule){

        $counter = 0;
        foreach( $package['contents'] as $line ) {
            if( static::match_line( $rule['value'], $line['product_id'], $line['data'] ) ) {
                $counter += 1;
            }
        }

        if($counter < count($package['contents'])){
            return true;
        }

        return $val;

    }

    public static function do_none($val, $package, $rule){

        foreach( $package['contents'] as $line ) {
            if( static::match_line( $rule['value'], $line['product_id'], $line['data'] ) ) {
                return false;
            }
        }

        return true;

    }

    public static function do_1($val, $package, $rule){

        foreach( $package['contents'] as $line ) {
            if( static::match_line( $rule['value'], $line['product_id'], $line['data'] ) ) {
                return true;
            }
        }

        return $val;

    }

}