/**
 * Invisible Dragon DeliveryPlus
 */
(function($){

	$(document).ready(function() {

		// Show the most helpful description on this element based on it's value
		$("#woocommerce_deliveryplus_class_cost_mode").each(function(){
			let help = $("<p>").addClass("description").appendTo($(this).parent());
			$(this).on("change", function(){
				help.html( $(this).data( $(this).val() ) );
			}).trigger("change");
		});

		$(".id-filtering").each(function () {

			// When category is picked, load next option down
			$(this).on("change", ".id-filter-category", function () {
				var nextDrop = $(".id-filter-condition", $(this).closest("tr"));
				nextDrop.html('<option>...</option>');
				$.getJSON(window.ajaxurl + "?action=deliveryplus_get_conditions&category=" + $(this).val(), function (data) {
					nextDrop.html('');
					for(let key in data) {
						var nVal = data[key];

						$("<option>").attr("value", key).text(nVal['label']).attr("data-type", nVal['type']).appendTo(nextDrop);
					}
					if(nextDrop.attr("data-value")) {
						// If we need to pre-set, pre-set
						nextDrop.val(nextDrop.attr("data-value"));
						nextDrop.removeAttr("data-value");
					}
					// Load default values
					nextDrop.trigger('change');
				});
			});

			// When condition type is changed, update dropdown
			$(this).on("change", ".id-filter-condition", function () {
				// Get the type of the condition value
				var category = $(".id-filter-category", $(this).closest("tr")).val();
				var nextType = $("option:selected", $(this)).attr("data-type");

				var nextDrop = $(".id-filter-value", $(this).closest("tr"));
				var n = $("<div>").addClass("id-filter-value").text("...");
				nextDrop.replaceWith(n);
				nextDrop = n;

				if(nextType == 'pick'){

					$.getJSON(window.ajaxurl + "?action=deliveryplus_get_values&category=" + category +"&condition=" + $(this).val(), function (data) {
						var n = $("<select>").addClass("id-filter-value");
						nextDrop.replaceWith(n);
						nextDrop = n;
						for(let key in data) {
							$("<option>").attr("value", key).text(data[key]).appendTo(nextDrop);
						}
						if(nextDrop.parent().attr("data-value")) {
							nextDrop.val(nextDrop.parent().attr("data-value"));
							nextDrop.parent().removeAttr("data-value");
						}
						nextDrop.trigger('change');
					});

				} else if(nextType == 'value'){

					let n = $("<input>").addClass("id-filter-value input-text").attr("type", "text");
					nextDrop.replaceWith(n);
					nextDrop = n;
					if(nextDrop.parent().attr("data-value")) {
						nextDrop.val(nextDrop.parent().attr("data-value"));
						nextDrop.parent().removeAttr("data-value");
					}

				} else if(nextType == 'distance') {

					let n = $("<div>").addClass("id-filter-distance");
					nextDrop.replaceWith(n);
					nextDrop = n;

					let value = $("<input type='hidden' />").addClass("id-filter-value").appendTo(n);

					let km = $("<input type='text' />").addClass("input-text").attr("placeholder", "km").appendTo(n);
					let lat = $("<input type='text' />").addClass("input-text").attr("placeholder", "lat").appendTo(n);
					let lng = $("<input type='text' />").addClass("input-text").attr("placeholder", "lng").appendTo(n);
					$(".input-text", n).on("change update", function(){
						value.val( km.val() + "|" + lat.val() + "|" + lng.val() ).trigger("change");
					});

					if(nextDrop.parent().attr("data-value")) {
						let splitValue = nextDrop.parent().attr("data-value").split("|");
						km.val( splitValue[0] );
						lat.val( splitValue[1] );
						lng.val( splitValue[2] );
						nextDrop.parent().removeAttr("data-value");
					}

				} else {
					nextDrop.text("! " + nextType);
				}
			});

			// Delete Row
			$(this).on("click", ".id-remove-row", function(e){
				e.preventDefault();
				$(this).closest("tr").remove();
				renderJSON();
			});

			// Delete Block
			$(this).on("click", ".id-remove-block", function(e){
				e.preventDefault();
				$(this).closest(".id-filter-set").remove();
				renderJSON();
			});

			// New Row
			$(this).on("click", ".id-add-new-and", function() {
				newAndCondition($(this).closest("table"));
			});

			// New Block
			$(this).on("click", ".id-add-new-or", function(){
				newOrCondition();
			});

			function newAndCondition(table, row) {
				let template = $(".template", table);
				let n = template.clone().removeClass("template").addClass("real").insertBefore(template);

				if(row){
					$(".id-filter-category", n).val(row['category']);
					$(".id-filter-condition", n).attr("data-value", row['condition']);
					$(".id-filter-value", n).parent().attr("data-value", row['value']);
				}

				$(".id-filter-category", n).trigger('change');
			}

			function newOrCondition(data) {
				let template = $(".id-filter-set.template", ".id-filtering");
				let n = template.clone().removeClass("template").addClass("real").insertBefore(template);
				if(data) {
					$.each(data, function(i, row){
						newAndCondition(n, row);
					});
				} else {
					newAndCondition(n);
				}
				return n;
			}

			// Parse input from saved
			let items = JSON.parse($("input[type='hidden']", $(".id-filtering").closest('fieldset')).val() || '[]');

			if(items.length > 0) {
				$.each(items, function(k, item){
					newOrCondition(item);
				});
			}

			function renderJSON() {

				let ret = [];

				$(".id-filter-set.real").each(function() {
					let row = [];
					$("tr.real", this).each(function(){
						row.push({
							'category': $(".id-filter-category", this).val(),
							'condition': $(".id-filter-condition", this).val(),
							'value': $(".id-filter-value", this).val()
						});
					});
					ret.push(row);
				});

				$("input[type='hidden']", $(".id-filtering").closest('fieldset')).val(JSON.stringify(ret));

			}

			$(this).on("change update", "select, input[type='text']", function(){
				renderJSON();
			});

		});

	});

})(jQuery);
